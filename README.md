## Lumino Theme

Custom Theme for Frappe
To Install 
frappe-bench : bench get-app https://bitbucket.org/aramryu/lumino_theme.git 
frappe-bench : bench install-app lumino_theme

For unistalling 
frappe-bench : bench uninstall-app lumino_theme
#### License

MIT
