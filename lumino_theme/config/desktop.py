# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Lumino Theme",
			"color": "black",
			"icon": "octicon octicon-paintcan",
			"type": "module",
			"label": _("Lumino Theme")
		}
	]
