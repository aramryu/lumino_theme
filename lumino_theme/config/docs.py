"""
Configuration for docs
"""

# source_link = "https://bitbucket.org/aramryu/lumino_theme"
# docs_base_url = "https://bitbucket.org/aramryu/lumino_theme/docs"
# headline = "App that does everything"
# sub_heading = "Yes, you got that right the first time, everything"

def get_context(context):
	context.brand_html = "Lumino Theme"
